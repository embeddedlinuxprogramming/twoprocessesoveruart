//
// Created by Juan Bernardo Gómez Mendoza on 11/4/23.
//

#include <pthread.h>
#include <stdio.h>

#include "main.h"

int main(int argc, char* argv[]) {
    // Variable definition.
    size_t taskNum[2] = { 1, 2};
    pthread_t task[2];
    // Create threads A and B.
    int retA = pthread_create(task, NULL, taskMethod, taskNum);
    int retB = pthread_create(task + 1, NULL, taskMethod, taskNum + 1);
    // Join both tasks.
    pthread_join(task[0], NULL);
    pthread_join(task[1], NULL);
    return 0;
}
