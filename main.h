//
// Created by Juan Bernardo Gómez Mendoza on 11/4/23.
//

#ifndef TWOPROCESSESOVERUART_MAIN_H
#define TWOPROCESSESOVERUART_MAIN_H

void sendByUART(char *);
void* taskMethod(void *);

#endif //TWOPROCESSESOVERUART_MAIN_H
