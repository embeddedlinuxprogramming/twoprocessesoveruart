//
// Created by Juan Bernardo Gómez Mendoza on 11/4/23.
//

#include <stdio.h>
#include <pthread.h>
#include "main.h"

pthread_mutex_t UARTMutex;

void* taskMethod(void* parm) {
    int sentMessages = 0;
    int missedMessages = 0;
    // Process input parameter.
    int* taskNumPtr = parm;
    int taskNum = *taskNumPtr;
    char message[80];
    sprintf(message, "-> Message from task %d.", taskNum);
    while(1) {
        if(pthread_mutex_trylock(&UARTMutex)) {
            sentMessages++;
            sendByUART(message);
            pthread_mutex_unlock(&UARTMutex);
            printf("[I] Task %d sent a message (%d).\n\r", taskNum, sentMessages);
        }
        else {
            missedMessages++;
            fprintf(stderr, "[E] Task %d missed a message (%d).\n\r",
                    taskNum, missedMessages);
        }
    }
}